<?php
include "db.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	 
</head>
<body>
<table border="1" cellpadding="10px" width="30%" align="center" bgcolor="#3399CC" font-weight="bold">
<form method="post" enctype="multipart/form-data">	
<th colspan="2"><h2>Insert New Product</h2></th>
	<tr><td class="label">Product Title</td><td><input type="text" name="product_title"></td></tr>
	<tr><td class="label">Product Category</td><td>
		<select name="product_category">
			<option>Select Category</option>
			<?php 
				$result= DB::getDbInstance()->getData("categories");
				while($row=mysqli_fetch_array($result)){
					$cat_id=$row['cat_id'];
					$cat_title=$row['cat_title'];
					echo "<option value=$cat_id >$cat_title</option>";
				}

			?>
			
		</select>
	</td></tr>
	<tr><td class="label">Product Brand</td><td>
		<select name="product_brand">
			<option>Select Brand</option>
			<?php $result=DB::getDbInstance()->getData("brand");
			while ($row=mysqli_fetch_array($result)) {
				$brand_id=$row['brand_id'];
				$brand_title=$row['brand_title'];
				echo "<option value=$brand_id>$brand_title</option>";
			}
			?>
		</select></td></tr>
	<tr><td class="label">Product Image</td><td><input type="file" name="product_image"></td></tr>
	<tr><td class="label">Product Price</td><td><input type="text" name="product_price"></td></tr>
	<tr><td class="label">Product Description</td><td><textarea rows="10" col="20" name="product_desc"></textarea></td></tr>
	<tr><td class="label">Product Keyword</td><td><input type="text" name="product_keyword" ></td></tr>
	<th colspan="2"><input type="submit" name="submit" value="Submit"></th>
	</form>
</table>

</body>
</html>
<?php 
if (isset($_POST['submit'])) {
	
	$product_title=$_POST['product_title'];
	$product_category=$_POST["product_category"];
	$product_brand=$_POST["product_brand"];
	$img_name=$_FILES['product_image']['name'];
	$img_temp=$_FILES['product_image']["tmp_name"];
	move_uploaded_file($img_temp, "images/$img_name");
	$product_price=$_POST["product_price"];
	$product_keyword=$_POST["product_keyword"];
	$product_desc=$_POST["product_desc"];
	if($product_title=="" or $product_category)
	$query= "Insert into product (cat_id,brand_id,product_image1,product_title,product_price,product_status,product_description,date) values('$product_category','product_brand','$img_name','$product_title','$product_price','$product_keyword','$product_desc',NOW())";
	$result=DB::getDbInstance()->setProduct($query);
	if(isset($result))
	{
		echo "<script>alert('Product has been added successfully')</script>";
	}

}

?>
<?php 
function get_product()//to get All product according to selected category called in index.php
{
	if(!isset($_GET["cat_id"]))
	{
		if(!isset($_GET["brand_id"]))
		{
			$result=DB::getDbInstance()->getData("product");
			creat_prodLayout($result);
		}
	}
					
}

function get_catProduct()//to get the product according to selected category called in index.php
{
	if(isset($_GET["cat_id"]))
	{
		$cat_id=$_GET["cat_id"];
		$result=DB::getDbInstance()->get_product("product",$cat_id);
		creat_prodLayout($result);

	}
}


function get_brandProduct()//to get the product according to selected brand called in index.php
{
	if(isset($_GET["brand_id"]))
	{
		$brad_id=$_GET["brand_id"];
		$result=DB::getDbInstance()->get_brandproduct("product",$brad_id);
		creat_prodLayout($result);

	}
}

function creat_prodLayout($result)//to display product in proper format
{
	while ($row=mysqli_fetch_array($result)) {
						$product_title=$row['product_title'];
						$product_image=$row['product_image1'];
						$product_price=$row['product_price'];
						$product_id=$row['product_id'];
						echo "<div id='prodct_display'>
						<h3>$product_title</h3><p><img src='images/$product_image' id='product_image' ><center>Price:&nbsp;$product_price/-</center></p><a href='#'' >Details</a>
						<a href='index.php?addToCart=$product_id'><button style='float:right'>Add to cart</button></div> ";

					}
}

function addItemToCart(){//to add item to the card called in index.php

	if(isset($_GET["addToCart"]))
	{
		
		$ip=getIP();
		$p_id=$_GET["addToCart"];
		$count=1;
		$query="select * from cart where p_id=$p_id and ip_add='$ip'";
		$result=DB::getDbInstance()->getProductQuantity($query);
		$product_count=mysqli_num_rows($result);
		
		if($product_count>0)
		{
			$row=mysqli_fetch_array($result);
			$count=$row['quantity']+1;
			$query="update cart set quantity=$count where p_id=$p_id and ip_add='$ip'";
			
		}
		else
			$query="Insert into cart(p_id,ip_add,quantity) values($p_id,'$ip',$count);";

		DB::getDbInstance()->getProductQuantity($query);
	}
	
}
function getIP()//to get the user's ip address so that system can generate the bill based on IP address in case of multiple users
{
	$ip=$_SERVER["REMOTE_ADDR"];
	if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
		$ip=$_SERVER["HTTP_CLIENT_IP"];
	}
	elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
		$ip=$_SERVER["HTTP_X_FORWARDED_FOR"];
	}
	return $ip;
}
function getTotalItem(){//to get the count of items selected by user called in index.php
	$count=0;
	$ip=getIP();
	$query="select * from cart where ip_add='$ip'";
	$result=DB::getDbInstance()->getProductQuantity($query);
	$count=mysqli_num_rows($result);
	return $count;
}

function totalPrice(){//to get the total of items selected by user called in index.php
	$ip=getIP();
	$total=0;
	$query= "select c.quantity,p.product_price,c.p_id from cart c, product p where c.p_id=p.product_id and c.ip_add='$ip'";
 	$result=DB::getDbInstance()->getProductQuantity($query);
 	if (mysqli_num_rows($result)>0) {
 		while ($row=mysqli_fetch_array($result)) {
 			$quantity=$row['quantity'];
 			$price=$row['product_price'];
 			$total+=$quantity*$price;
 		}
 	}
 	return $total;
}

function getCartInfo(){//to show the cart to the user 
	$ip=getIP();
	$query= "select c.quantity,p.product_price,c.p_id,p.product_image1,p.product_title from cart c, product p where c.p_id=p.product_id and c.ip_add='$ip'";
 	return DB::getDbInstance()->getProductQuantity($query);
 }

 function showCart()
 {
 		$result=getCartInfo();
 	if(mysqli_num_rows($result)>0)
							{
								$price=0;
								echo "<tr><th><h3>Remove</th><th>Product</th><th>Quantity</th><th>Price</th></h3></tr>";
								while($row=mysqli_fetch_array($result))
								{
								$product_img=$row["product_image1"];
								$product_title=$row["product_title"];
								$p_id=$row["p_id"];
								$prdt_qty=$row["quantity"];
								$product_price=$row["product_price"];
								$price+=$prdt_qty*$product_price;
								echo "<tr><td><input type='checkbox' name='check_list[]'  value='$p_id'></td>
									<td><h4>$product_title</h4><img src='images/$product_img'></td>
								<td><input type='text' name='$p_id' value=$prdt_qty></td>
								<td>$product_price</td></tr>";
								}
								echo "<tr><td></td><td></td><td></td><td colspan='3' style='color:yellow'><h3>Total : &nbsp;&nbsp;$price</h3></td></tr>
								<tr><td></td><td><button ><a href='index.php'>Continue Shopping</a></button></td><td><input type='submit' name='update_cart' value='Update Cart'></button></td><td><button>Make payment</button></td></tr>";
							}
							else 
							{
								echo "<tr><td><h3>Your Cart is Empty!<h3></td></tr>
								<tr><td><img src='images/empty_cart.jpeg'height='150px' width='150px'><td></tr>
						    	<tr><td><a href='index.php'><button type='button'>Continue Shopping</button></a></td></tr>";
							}
 }

 function remove_product($selected)
{
 	$ip=getIP();
 	$query="delete from cart WHERE p_id = $selected and ip_add='$ip'";
 	return DB::getDbInstance()->getProductQuantity($query);

 }
 function update_quantity($p_id,$quantity)
 {
 	$ip=getIP();
 	$query=" update cart set quantity = $quantity WHERE p_id = $p_id and ip_add='$ip'";
 	return DB::getDbInstance()->getProductQuantity($query);
 }

 ?>
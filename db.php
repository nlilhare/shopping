<?php
class DB
{
    public $conn;
    public $db;
    function __construct()
    {
        include("config.php");
        $this->conn=mysqli_connect($servername,$username,$password,$dbname);
    }
    function getData($table_name)
    {
      
       $query="select * from ".$table_name;
       return mysqli_query($this->conn,$query);
    }
    static function getDbInstance()
    {
        if(!isset($db))
        {
            $db= new DB();
            return $db;
        }
        else
        return $db;  
    }
    function setProduct($query){

        return mysqli_query($this->conn,$query);
    }

    function get_product($product,$cat_id){
        $query= "select * from ".$product." where cat_id='".$cat_id."'";
        return mysqli_query($this->conn,$query);
    }
    function get_brandproduct($product,$brad_id)
    {
        $query= "select * from ".$product." where brand_id='".$brad_id."'";
         return mysqli_query($this->conn,$query);
    }
    function getSearch($search){
        $query="select * from product where product_status like '%"."$search%'";
         return mysqli_query($this->conn,$query);
    }
    function getProductQuantity($query){
            return mysqli_query($this->conn,$query);
        
    }
}

?>
<?php include "db.php"; 
include "product_info.php";?>
<!DOCTYPE html>
<html>
<head>
	<title>E-Shopping Website</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
<div class="wrapper">

	<div class="container-header" >

		<?php include "header.php" ?>

	</div>
	<div class="navigation-panel">
		
			<ul id="menu">
				<li><a href="index.php">Home</a></li>
				<li><a href="index.php">All Products</a></li>
				<li><a href="#"> My Account</a></li>
				<li><a href="#"> Sign Up</a></li>
				<li><a href="#"> Shopping Cart</a></li>
				<li><a href="#"> Contact us</a></li>
				<li><a href="#"> Feedback</a></li>
			</ul>
			<div id="search">
				<form method="post" action="search.php" enctype="multipart/form-data">
					<input type="text" name="search" placeholder="Search a product">
					<input type="submit" name="submit" value="Submit">
				</form>
			</div>
	
	</div>
	<div class="container-body">
			<div class="left-container">
				<div id="categories">
					<h2>Categories</h2>
					<ul id="product_category">
					<?php $result=DB::getDbInstance()->getData("categories");

						while ($row=mysqli_fetch_array($result)){
							$cat_id=$row['cat_id'];
							$cat_title=$row['cat_title'];
						echo "<li><a href='index.php?cat_id=$cat_id'>$cat_title</a></li>";
						
						}?>
					</ul>
				</div>
				<div id="categories">
					<h2>Brands</h2>
					<ul id="product_category">
					<?php $result= DB::getDbInstance()->getData("brand");
						while ($row=mysqli_fetch_array($result)) {
						$brand_id=$row['brad_id'];
							$brand_title=$row['brand_title'];
						echo "<li><a href='index.php?brand_id=$brand_id'>$brand_title</a></li>";	
						}

					?>	
					</ul>
				</div>
			</div>
			<div class="right-container">
				<div id="welcom_banner">

					<h3>Welcome Gueest! &nbsp;Shopping cart&nbsp;-&nbsp;Item:&nbsp;<?php echo getTotalItem();//to get the count of items selected by user?>&nbsp;-&nbsp;Price:&nbsp;<?php echo totalPrice();//to get the total of items selected by user defined in product_info.php?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="Cart.php">Go to Cart</a></h3>
				</div>

			</div>
		
				<?php  

						$search=$_POST["search"];
					$result=DB::getDbInstance()->getSearch($search);
					creat_prodLayout($result);
					
					
				?>

			
	</div>
	
</div>
</body>
</html>
<?php include "db.php"; 
include "product_info.php";?>
<!DOCTYPE html>
<html>
<head>
	<title>E-Shopping Website</title>
	<link rel="stylesheet" type="text/css" href="style/style.css"/>
</head>
<body>
<div class="wrapper">

	<div class="container-header" >

		<?php include "header.php" ?>

	</div>
	<div class="navigation-panel">
		
			<ul id="menu">
				<li><a href="index.php">Home</a></li>
				<li><a href="index.php">All Products</a></li>
				<li><a href="#"> My Account</a></li>
				<li><a href="#"> Sign Up</a></li>
				<li><a href="#"> Shopping Cart</a></li>
				<li><a href="#"> Contact us</a></li>
				<li><a href="#"> Feedback</a></li>
			</ul>
			<div id="search">
				<form method="post" action="search.php" enctype="multipart/form-data">
					<input type="text" name="search" placeholder="Search a product">
					<input type="submit" value="Submit">
				</form>
			</div>
	
	</div>
	<div class="container-body">
			<div class="left-container">
				<div id="categories">
					<h2>Categories</h2>
					<ul id="product_category">
					<?php $result=DB::getDbInstance()->getData("categories");// to render the category list on index page 

						while ($row=mysqli_fetch_array($result)){
							$cat_id=$row['cat_id'];
							$cat_title=$row['cat_title'];
						echo "<li><a href='index.php?cat_id=$cat_id'>$cat_title</a></li>";
						
						}?>
					</ul>
				</div>
				<div id="categories">
					<h2>Brands</h2>
					<ul id="product_category">
					<?php $result= DB::getDbInstance()->getData("brand");// to render the brand list on index page
						while ($row=mysqli_fetch_array($result)) {
						$brand_id=$row['brad_id'];
							$brand_title=$row['brand_title'];
						echo "<li><a href='index.php?brand_id=$brand_id'>$brand_title</a></li>";	
						}

					?>	
					</ul>
				</div>
			</div>
			<div class="right-container">
			<?php addItemToCart();//to add item to the card defined in product_info.php?> 
				<div id="welcom_banner">

					<h3>Welcome Gueest! &nbsp;Shopping cart&nbsp;-&nbsp;Item:&nbsp;<?php echo getTotalItem();//to get the count of items selected by user?>&nbsp;-&nbsp;Price:&nbsp;<?php echo totalPrice();//to get the total of items selected by user defined in product_info.php?>&nbsp;&nbsp;</h3>
				</div>
				<div id='cart_display'>
				<form method="post" action="cart.php">

				<table id="show_card">
					<?php  
					if(isset($_POST['update_cart']))
					{
						if(!empty($_POST['check_list']))
 						{ 	$result=false;
							foreach($_POST['check_list'] as $selected)
							{
								$result=remove_product($selected);
								
							}
							if($result)
								{
									echo "<script> window.open('Cart.php','_self');</script>";
								}
						}
						/*$result=getCartInfo();
						if(mysqli_num_rows($result)>0)
						{
							while($row=mysqli_fetch_array($result))
							{
								$p_id=$row['p_id'];
								//echo "<script>alert('$p_id');</script>";
								$quantity=$_POST['$p_id'];
								$result=update_quantity($p_id,$quantity);
								if($result)
								{
									echo "<script>alert('cart has been updated');</script>";
								}
							}
						}*/
						else
						showCart();
					}
					else{
							showCart();
						}
					
					?>
				</table>
				</form>
				</div>
			</div>
			
			
	</div>
	
</div>
</body>
</html>